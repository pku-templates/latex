\section{Reconstruction and selection}%
\label{sec:reconstruction_and_selection}

For the \Xiccp signal and each of the normalisation modes,
\Lc candidates are reconstructed in the $\proton\Km\pip$ final state.
At least one of the three \Lc decay products is 
required to pass an inclusive software trigger,
which requires that a track with associated large transverse momentum is inconsistent with originating from any PV.
For data recorded at $\sqrt{s}=13\tev$,
at least one of the three \Lc decay products is 
required to pass a multivariate selection
applied at the software trigger level~\cite{pmlr-v14-gulin11a, LHCb-PROC-2015-018}.
The \chisqip is defined as the difference in \chisq
of the PV fit with and without the particle in question.
The PV of any single particle is defined to be that with respect to which
the particle has the smallest \chisqip.
Candidate $\Lc$ baryons are formed from the combination of three tracks of good quality that do not originate from any PV and have large transverse momentum.  
Particle identification (PID) requirements are imposed on all three
tracks to suppress combinatorial background and misidentified charm-meson decays.
%by a distance corresponding to a proper decay time greater than 150\fs.
The \Lc candidates are also required to
have a mass in the range from 2211 to 2362\mevcc.
%The same \Lc selection is used for the signal and normalisation modes.

The \Xiccp candidates are reconstructed 
by combining a \Lc candidate with two tracks,
identified as \Km and \pip mesons using PID information.
The kaon and pion tracks are required to 
have a large transverse momentum and a good track quality.
To suppress duplicate tracks,
the angle between each pair of the five final-state tracks with the same charge is
required to be larger than 0.5\mrad.
The \Xiccp candidate is required to have $\pt>4\gevc$ and to originate from a PV.
Similar requirements are imposed to reconstruct the \Xiccpp candidates
in the \Xiccpp normalisation mode,
with an additional charged pion in the final state.

Multivariate classifiers based on the
gradient boosted decision tree (BDTG)~\cite{Breiman,Hocker:2007ht,*TMVA4} are developed to further improve the signal purity. 
To train the classifier, 
simulated \Xiccp events are used as the signal sample and 
wrong-sign (WS) $\Lc\Km\pim$ combinations selected from the data sample
are used as the background sample.
For \mbox{Selection A},
the classifier is trained using candidates
with a $\Lc$ mass in the 
window of 2270 to 2306\mevcc
(corresponding to $\pm3$ times the resolution on the \Lc mass)
and a \Xiccp mass in the
signal search region.
Eighteen input variables that show good discrimination
for \Xiccp and intermediate \Lc candidates between signal and background samples
are used in the training.
These variables can be 
subdivided into two sets;
in the choice of 
the first set of variables, no strong assumptions are made
about the source of the \Lc candidates, while for the second set of variables the properties of the \Xiccp candidates
as the source of the \Lc candidates are exploited.
The first set of variables are:
% for MVA1
the \chisq per degree of freedom of the \Lc vertex fit;
the \pt of the \Lc candidate and of its decay products;
and the flight-distance \chisq between the PV and the decay vertex
of the \Lc candidate. 
The second set of variables are:
% below for MVA2
the \chisq per degree of freedom of the \Xiccp vertex fit and of
the kinematic refit~\cite{Hulsbergen:2005pu} of the decay chain
requiring \Xiccp to originate from its PV;
the largest distance of closest approach (DOCA) between the decay products
of the \Xiccp candidate;
the \pt of the \Xiccp candidate, and of the kaon and pion from the \Xiccp decay;
the $\chisqip$ of the \Xiccp and \Lc candidates, and of the \Km and \pip mesons from the \Xiccp decay;
the angle between the momentum and displacement vector of the \Xiccp candidate;
and the flight-distance \chisq between the PV and the decay vertex
of the \Xiccp candidate.
For \mbox{Selection B},
the multivariate selection comprises two stages.
In the first stage,
one classifier is trained with $\Lcp$ signal in the simulated $\Xiccp$ sample and 
background candidates
in the \Lc mass sideband, 
and is applied to both the signal mode and the \Lc normalisation mode.
The same input variables are used as for the first set of variables in \mbox{Selection A},
with five additional variables that enhance the discriminating power:
the largest DOCA between the decay products
of the \Lc candidate
and the $\chisqip$ of the decay products of the \Lc candidate.
In the second stage,
another classifier is trained for the signal mode using candidates
in the mass window of the intermediate \Lc and the \Xiccp signal search region.
Candidates used in the training are also required to
pass a BDTG response threshold of the first classifier.
The input variables are those from the second set of \mbox{Selection A}
with an additional variable, 
the angle between the momentum and displacement vector of the \Lc candidate.

The thresholds of the BDTG responses for both \mbox{Selection A} and B
are determined by maximising the expected value of the figure of merit
$\varepsilon/\left(\frac{5}{2} + \sqrt{N_B}\right)$~\cite{Punzi:2003bu},
where $\varepsilon$ is the estimated signal efficiency, 
$5/2$ corresponds to 5 standard deviations in a Gaussian significance test, 
and $N_B$ the expected number of background candidates
under the signal peak.
The quantity $N_B$ is estimated with the WS control sample
in the mass region of $\pm12.5\mevcc$ around the known \Xiccpp mass~\cite{PDG2018},
taking into account the difference of the background level for the signal sample
and the WS control sample.
The performance of the BDTG classifier is tested and found to be stable against the $\Xiccp$ lifetimes in the range from 40 to 120\fs. 
Following the same procedure,
a two-stage multivariate selection is developed for the \Xiccpp normalisation mode.

Events that pass the multivariate selection may contain 
more than one \Xiccp candidate in the search region although the probability to produce more than one \Xiccp is small.
According to studies of simulated decays and the WS control sample,
multiple candidates in the same event do not form a peaking background except for one
case in which the candidates are obtained from the same five final-state
tracks, but with two tracks interchanged (e.g. the \Km from the \Lc decay and 
the \Km from the \Xiccp decay).
In this case,
only one candidate is chosen randomly.

For \mbox{Selection B},
an additional hardware trigger requirement is imposed on candidates of both the signal
and the normalisation mode 
to minimise systematic differences in efficiency between the modes. This hardware trigger requirement 
selects candidates in which at least one of the three \Lc decay products 
deposits high transverse energy in the calorimeters. Finally, 
\Xiccp baryon candidates in the signal mode and \Lc and \Xiccpp
baryons in the normalisation modes
are required to be reconstructed in the fiducial region of
rapidity $2.0<y<4.5$ and transverse momentum $4<\pt<15\gevc$.


