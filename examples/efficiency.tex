\section{Efficiency ratio measurement}%
\label{sec:efficiency_ratio}

 To set upper limits on the production ratios, the efficiency ratio $\eps_{\rm norm}/\eps_{\rm sig}$ is determined from simulation.
The signal efficiency is estimated with mass and lifetime
hypotheses of $m(\Xiccp)=3621\mevcc$ and $\tau(\Xiccp)=80\fs$.
The kinematic distribution of the \Xiccp baryon is assumed to be the same
as for its  isospin partner \Xiccpp and the \pt distribution of simulated
\Xiccp decays is corrected according to the data-simulation discrepancy
observed in the \Xiccpp normalisation mode.
The Dalitz distributions of the simulated \Lc decays are
corrected to match the distribution observed in  background-subtracted data, obtained using 
 the \sPlot technique~\cite{Pivk:2004ty}.
Corrections are applied to the tracking efficiency and PID response of the simulated samples using calibration data samples~\cite{LHCb-DP-2013-002,LHCb-PUB-2016-021,LHCb-DP-2018-001}. 
The efficiency ratio obtained for the \Lc and \Xiccpp normalisation modes
and for different data-taking years are summarised in Table~\ref{tab:eff_ratio},
where the uncertainties are due to the limited sizes of the simulated
samples. The increase in the efficiency ratio of the \Xiccpp normalisation mode in 2017--2018 compared to that in 2016
is due to the improvement of the online event selection following the observation of the \Xiccpp baryon.
\begin{table}[tb]
  \centering
  \caption{Efficiency ratios between the normalisation and signal modes for 
  different data-taking periods.
  The uncertainties are due to the limited size of the simulated samples.}
  \label{tab:eff_ratio}
    \begin{tabular}{l c c c c}
    \toprule
    & 2012 & 2016 & 2017 & 2018 \\
    \midrule
    $\varepsilon_{\mathrm{norm}}(\Lc)/\varepsilon_{\mathrm{sig}}$     &  54  $\pm$  17    & 22.0  $\pm$  1.9\;\;   & 22.4  $\pm$  1.3\;\;    & 26.1  $\pm$  1.8\;\;  \\
    $\varepsilon_{\mathrm{norm}}(\Xiccpp)/\varepsilon_{\mathrm{sig}}$ & 2.1  $\pm$  0.7   & 1.17  $\pm$  0.11  & 1.91  $\pm$  0.11  & 1.99  $\pm$  0.12 \\
    \bottomrule
  \end{tabular}
\end{table}

The signal efficiency of the event selection has a strong dependence on
the \Xiccp lifetime. To estimate the efficiency for
other lifetime hypotheses, the decay time of the simulated \Xiccp events
are weighted to have different exponential distributions
and the efficiency is re-calculated.
A discrete set of hypotheses (40\fs, 80\fs, 120\fs, and 160\fs)
is motivated by the measured \Xiccpp lifetime of 256\fs~\cite{LHCb-PAPER-2018-019} and 
the expectation that the \Xiccp lifetime is 
three to four times smaller than that of the \Xiccpp baryon~\cite{
  Fleck:1989mb,
  guberina1999inclusive,
  Kiselev:1998sy,
  Likhoded:1999yv,
  Onishchenko:2000yp,
  Anikeev:2001rk,
  Kiselev:2001fw,
  hsi2008lifetime,
  Karliner:2014gca,
  Berezhnoy2016}.
Combining the yields of the normalisation modes obtained in the previous section,
the values of the single-event sensitivity of the 
\Lc and \Xiccpp modes 
for several lifetime hypotheses are shown in 
Table~\ref{tab:alpha_Lc} and Table~\ref{tab:alpha_Xicc},  respectively.
The uncertainties on the single-event sensitivities are due to the limited sizes of the simulated samples
and the statistical uncertainties on the measured yields.
\begin{table}[tb]
  \centering
  \caption{Single-event sensitivity of the \Lc normalisation mode
  $\alpha(\Lc)$ [$\times 10^{-5}$]
  for different lifetime hypotheses of the $\Xiccp$ baryon in the different data-taking years.
  The uncertainties are due to the limited sizes of the simulated samples
  and the statistical uncertainties on the measured \Lc baryon yields.}
\label{tab:alpha_Lc}
\begin{tabular}{c cccc} 
\toprule 
 & $\tau=40\fs$ & $\tau=80\fs$ & $\tau=120\fs$ & $\tau=160\fs$ \\ 
\midrule 
2012& 14.2 $\pm$ 4.8\;\; & 4.6 $\pm$ 1.4& 2.65 $\pm$ 0.77& 1.91 $\pm$ 0.53\\ 
2016& 0.60 $\pm$ 0.08& 0.29 $\pm$ 0.02& 0.20 $\pm$ 0.01& 0.16 $\pm$ 0.01\\ 
2017& 0.46 $\pm$ 0.04& 0.23 $\pm$ 0.01& 0.15 $\pm$ 0.01& 0.12 $\pm$ 0.01\\ 
2018& 0.52 $\pm$ 0.04& 0.23 $\pm$ 0.02& 0.15 $\pm$ 0.01& 0.11 $\pm$ 0.01\\ 
\bottomrule 
\end{tabular} 
\end{table}


\begin{table}[tb]
  \centering
  \caption{Single-event sensitivity of the \Xiccpp normalisation mode 
  $\alpha(\Xiccpp)$ [$\times 10^{-2}$]
  for different lifetime hypotheses of the $\Xiccp$ baryon in the different data-taking years.
  The uncertainties are due to the limited size of the simulated samples
  and the statistical uncertainty on the measured \Xiccpp baryon yield.}
\label{tab:alpha_Xicc}
\begin{tabular}{c cccc} 
\toprule 
 & $\tau=40\fs$ & $\tau=80\fs$ & $\tau=120\fs$ & $\tau=160\fs$ \\ 
 \\ 
\midrule 
2012& 16.7 $\pm$ 7.1\phantom{7}& 5.4 $\pm$ 2.2& 3.1 $\pm$ 1.2& 2.3 $\pm$ 0.8\\ 
2016& 1.96 $\pm$ 0.42& 0.96 $\pm$ 0.18& 0.65 $\pm$ 0.12& 0.52 $\pm$ 0.09\\ 
2017& 2.51 $\pm$ 0.42& 1.25 $\pm$ 0.19& 0.84 $\pm$ 0.13& 0.69 $\pm$ 0.11\\ 
2018& 2.36 $\pm$ 0.34& 1.06 $\pm$ 0.15& 0.68 $\pm$ 0.10& 0.52 $\pm$ 0.08\\ 
\bottomrule 
\end{tabular} 
\end{table}

The efficiency could depend on the \Xiccp mass,
since it affects the kinematic distributions of the decay products of the \Xiccp baryon.
To test other mass hypotheses,
two simulated samples are generated with
$m(\Xiccp)=3518.7\mevcc$ and $m(\Xiccp)=3700.0\mevcc$.
The \pt distributions of the three decay products of the \Xiccp
in the simulated sample with $m(\Xiccp)=3621.4\mevcc$
are weighted to match those in the other mass hypotheses,
and the efficiency is re-calculated with the weighted sample.
Despite the variations of individual efficiency components,
the total efficiency is found to be independent of such variations.
The mass dependence can be effectively ignored for the evaluation of 
the single-event sensitivities.
